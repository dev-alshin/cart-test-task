/* Configuration */

// preselected cart
const selectedCart = [
  { price: 20 },
  { price: 45 },
  { price: 67 },
  { price: 1305 },
];

// list of currencies
const currencyList = [
  {
    name: 'rubles',
    code: 'RUB',
  },
  {
    name: 'euros',
    code: 'EUR',
  },
  {
    name: 'US dollar',
    code: 'USD',
  },
  {
    name: 'pounds',
    code: 'GBP',
  },
  {
    name: 'yens',
    code: 'JPY',
  },
];

// base currency to quote against
const BASE_CURRENCY = 'USD';

// URL for fetching currency rates
const API_BASE = 'http://api.fixer.io/latest';

/**
 * Fetches currency ratios
 * 
 * @param {object} codes - Currency codes
 * @param {string} base - Base currency to quote against
 * @param {string} api - URL for fetching currency rates
 * @return {object} Promise of currency rate
 */
function fetchCurrencyRates(codes, base, api) {
  const fetchStr = `${api}?base=${base}&symbols=${codes.join(',')}`;
  return fetch(fetchStr);
}

/**
 * Counts total cart price for given currencies
 * 
 * @param {object} cart - Cart array with prices
 * @param {object} currencyList - List with currencies to count in
 * @param {string} base - Base currency to quote against * 
 * @param {string} api - URL for fetching currency rates
 * @return {object} Total cart prices
 */
async function countSumInCurrencies(cart, currencyList, base, api) {
  // sums cart items prices
  const cartSum = cart.reduce((sum, item) => sum + item.price, 0);

  // fetches rates for given codes
  const codes = currencyList.map(currency => currency.code);
  const ratesPromise = await fetchCurrencyRates(codes, base, api);
  const ratesResponse = await ratesPromise.json();
  const rates = ratesResponse.rates;
  // Adds rate 1 for the base currency
  // Api does not returns its value
  // because it is always equals 1.
  rates[base] = 1;

  return currencyList.map(item => ({
    [item.name]: cartSum * rates[item.code],
  }));
}

// initiates counting process
countSumInCurrencies(
  selectedCart,
  currencyList,
  BASE_CURRENCY,
  API_BASE
).then(res => console.log(res));
